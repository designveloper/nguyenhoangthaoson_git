### 1. What is Git
   * Git is a version control system, use Git to keep track of changes.
   
   * Users don't have to work from a central repository, each repository is independent.
### 2. Install Git
   * Git can run on both Mac, Linux, Windows
   * Git configuration:
     - system level: ```git config --system```
     - users level: ```git config --global```
     - project level: ```~/project/.git/config```
### 3. Get started
   * init: ```git init```
   * make changes
   * commit changes: ```git add .```   
                     ```git commit "commit message"```
   * see commit log:```git log```
   
### 4. Git Concepts and Architecture
   * Git use three-tree architecture: repository- staging index - working directory
   * Head:   
	    - pointer to "tip" of current branch in repository        
	    - show last checked out / point to parent of next commit
        
### 5.Making Changes to Files
   * View changes has made: ```git status```
   * View changes with diff: ```git diff```
   * Deleting files: ```git rm "file_name" ```
   * Moving and renaming files:	```git mv <file_name_1><file_name_2>```
### 6. Using Git with a Real Project
   * View changes side by side: git diff --color-words file_name
   * Commit modified file, (untracked & deleted files does not include)

### 7. Undoing Changes
   * Undoing changes on file: ```git checkout -- file_name```
   * Unstaging file: ```git reset HEAD file_name```
   * Changing previous commit / changing commit message: ```git commit --amend "message"```
   * Retrieving old version:	```git checkout -- SHA1_Value file_name```
   * Reverting a commit: ```git revert SHA1_Value```
   * Undo commit: ```git reset <option> SHA1_Value```
   
	   - Three option:
        
	      + ```--soft```: not change staging index or working directory
          
	      + ```--mixed```: changes staging index, not change working directory
          
	      + ```--hard```: changes staging index and working directory
          
       - Removing untracked files: ```git clean```

### 8. Ignoring Files
   * Using .gitignore files:
	   - locate: project/.gitignore
	   - basic regular expressions: ? [aieou] [0-9]
	   - negative expression with !
	   - ignore all files in a directory: directoryA/
	   - comment lines begin with #, blank lines are skipped
   * What to ignore:
	   - compiled source code
	   - package & compressed files
	   - logs & databases
	   - OS generated files
	   - user uploaded asset (PDFs, images, videos)
   * Global ignore: ```git config --global core.excludesfile file_location ```
   * Ignoring tracked files: ```git rm --cached file_name ```
   * Tracking empty directory: ```touch directory/.gitkeep```
   		
### 9. Navigating the Commit Tree

   * Referencing commit:
   	   - full sha-1 hash
	   - short sha-1 hash
	      + at least 4 characters
	      + unambiguous (8-10)
	      + bigger project (depend on project size)
   	   - HEAD pointer
   	   - Branch reference, tag reference
   	   - Ancestry
   * Listing a tree: ```git ls-tree```
   * Getting more from log:
   	   - online: ```git log --online```
	   - period of time: ```git log --since="time specific"``` , ```git log --since="time specific" --until="time specific"```
	   - by author: ```git log --author="author_name" ```
   * Viewing commit: ```git show SHA1_Value```
   * Comparing commit: ```git diff <SHA1_Value>..SHA1_Value2``` 
### 10. Branching
   * View Branch: ```git branch```
   * Creating Branch: ```git branch new_branch_name```
   * Switching Branch: ```git checkout branch_name```
   * Creating & switching Branch: ```git checkout -b branch_name```
   * Comparing branch: ```git diff branch1..branch2```
   * Renaming branch: ```git branch -m old_branch_name new_branch_name```
   * Deleting branch: ```git branch -d brach_to_be_delete```
   	   - Cannot delete current branch
	   - Cannot delete branch that has changes that aren't merged
	    
### 11. Merging branch
   * Merging branch: ```git merge brand_to_merge```
   * Conflict: two changes occur on same lines on two commit
   * Resolving conflict: 
   	   - Abort merge
	   - Resolve manually
	   - Use merge tool
   * Strategies reduce merge conflict:
   	   - keep lines short
	   - keep commit small & focused
	   - beware stray edit (blank space, tab)
	   - merge often
### 12. Stashing change
   * Saving change in stash: ```git stash save "message"```	
   * Viewing stash: ```git stash list```
   * Retrieving stash:
   	   - Pull the stash and leave the copy: ```git stash apply```
	   - Pull the stash and remove from stash: ```git stash pop```
	   - Delete stash items: ```git stash drop stash item```
	   - Delete everything in the stash: ```git stash clear```
### 13. Remotes
   * Remote: Git repository stored on server, other people can see.
   * origin/master: Git repository stored on local machine, reference to remote branch server.	
   * Add remote: ```git remote add alias url```
   * Delete remote: ```git remote rm alias```
   * Create remote branch: ```Creating a remote branch: git push -u alias branch```
   * Clone a remote: ```git clone url```
   * Tracking remote branch: push with -u option on that branch
   * Push changes to remote: ```git push branch```
   * Delete a remote branch: ```git push origin :branch```, ```git push origin --delete branch```
### 14. Tools & next step
   * Git hosting: github, bitbucket, gitorious
